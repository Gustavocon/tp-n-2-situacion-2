package Situacion;

public class Club   //Clase encargada de administrar los pagos de los socios del Club
{
    public int realizarPago(Socio socio) //Metodo encargado del cobro de la cuota
    {
        if (socio.getVitalicio() == false)  //Solo se cobra si el socio no es vitalicio
        {
            if ((socio.getAniosAntiguedad() > 20) || (socio.getAniosAntiguedad() == 20 && socio.getMesesAntiguedad() > 1))
            {
                return (int)(1500*0.5);
            }
            else
            {
                return 1500;
            }
        }
        else
        {
            return 0;
        }
    }

    public boolean generarComprobantePago(Socio socio)
    {
        if (socio.getVitalicio() == false)  //Solo se genera un comprobante de pago, si el socio no es vitalicio
        {
            System.out.println("Nombre: " + socio.getNombre());
            System.out.println("Apellido: " + socio.getApellido());
            System.out.println("DNI: " + socio.getDNI());
            System.out.print("Socio: ");
            
            if (socio.getAniosAntiguedad() > 20)
            {
                System.out.println("adherente");
            }
            else
            {
                System.out.println("comun");
            }
            System.out.println("Importe: " + realizarPago(socio) + "$");
            System.out.println("Cantidad de cuotas pagadas: " + (socio.getAniosAntiguedad() * 12 + socio.getMesesAntiguedad()));

            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean generarComprobanteMensual(Socio socio)
    {
        if (socio.getVitalicio() == true) //Solo se genera un comprobante mensual, si el socio es vitalicio
        {
            System.out.println("Documento de socio vitalicio: " + socio.getDNI());
            return true;
        }
        else
        {
            return false;
        }
    }

}